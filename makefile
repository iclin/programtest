all:
	g++ -c main.cc
	g++ -o main main.cc
clean:
	@rm main
	@rm *.o
test:
	$(MAKE) -C case1 test
	$(MAKE) -C case2 test
	$(MAKE) -C case3 test
